Hook
=============================

Hook for the [git](https://git-scm.com/ "Git version control system") repository, which uses the [core part](https://bitbucket.org/ok-test-autoteam/tflr-core "Core part of the TFLR project") to verify the project before the commit.

We use our own validation jar to validate Java files. You can either install the IntelliJ plugin or use a pre commit hook to automatically re-validate files.

###How to add a pre commit hook:

Build validation jar.

Go to your repository root directory: 

    $ cd <<REPO_ROOT>>

Copy /config/pre-commit to .git/hooks: 

    $ cp /config/pre-commit .git/hooks/

Make the script executable: 

    $ chmod +x .git/hooks/pre-commit

Replace <<JAR_PATH>> in .git/hooks/pre-commit with the location of validation jar. 

Code should compile without warnings. To enforce this, we use a combination of -Xlint and -Werror flags to javac in addition to a number of other static checks provided by PMD.
Warnings should either be fixed or annotated with @SuppressWarnings("type-of-warning"), as appropriate.



#### How to add changes to the stub application and compile it

Go to your repository root directory:  
    
    $ cd <<REPO_ROOT>>
 
Compile main class: 

    $ javac -cp . /src/main/java/app/StubApplication.java

Create executable jar: 

    $ cp src/main/java/app/StubApplication.class /classes/app/

    $ jar -cvfm stub.jar /classes/manifest.txt /classes/app/*.class
    
Add some files with name matched "Bad*.java" to repository 

Test: $ git commit -m 'new'. You will find something like file 

    src/main/resources/BadCodeStyleFile.java is not valid:
          This file has bad code style

package app;

import java.io.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

public class StubApplication {

    private static BiConsumer<File, File> validateCodeStyle =
            (rf, wf) -> {
                if (rf.getName().matches("Bad.*")) {
                    try (FileWriter fileWriter = new FileWriter(wf)){
                        fileWriter.write("This file has bad code style\n");
                        fileWriter.flush();
                    } catch (IOException e) {
                        throw  new UncheckedIOException(e);
                    }
                }
            };

    private static BiFunction<String[], Integer, File> parseArgs = (f, i) -> new File(f[i]);


    public static void main(String[] args) throws UncheckedIOException {
        if (args.length != 0) {
            validateCodeStyle.accept(parseArgs.apply(args, 0), parseArgs.apply(args, 1));
        } else {
            System.err.println("there is no argument");
            System.exit(0);
        }

    }

}
